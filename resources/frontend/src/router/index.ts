import Vue from 'vue';
import VueRouter from 'vue-router';
import Authentication from "@/config/Authentication";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        meta: {deniedForAuth: true},
        name: 'Login',
        component: () => import("@/views/LoginPage.vue"),
    },
    {
        path: '/',
        name: 'Layout',
        meta: {requiredAuth: true},
        component: () => import("@/views/LayoutPage.vue"),
        children: [
            {
                path: '/home',
                name: 'Home',
                meta: {requiredAuth: true},
                component: () => import("@/views/HomePage.vue"),
            },
            {
                path: '/master-dosen',
                name: 'MasterDosen',
                meta: {requiredAuth: true},
                component: () => import("@/views/MasterDosenPage.vue"),
            },
            {
                path: '/profile/:email',
                name: 'Profile',
                meta: {requiredAuth: true},
                component: () => import("@/views/ProfilePage.vue"),
            },
            {
                path: '/master-mapel',
                name: 'MasterMapel',
                meta: {requiredAuth: true},
                component: () => import("@/views/MasterMapelPage.vue"),
            },
            {
                path: '/master-kelas',
                name: 'MasterKelas',
                meta: {requiredAuth: true},
                component: () => import("@/views/MasterKelasPage.vue"),
            },
            {
                path: '/course-avalaible',
                name: 'CourseAvalaible',
                meta: {requiredAuth: true},
                component: () => import("@/views/CourseAvalaiblePage.vue"),
            },
            {
                path: '/class-teached',
                name: 'ClassTeached',
                meta: {requiredAuth: true},
                component: () => import("@/views/ClassTeachedPage.vue"),
            },
            {
                path: '/soals',
                name: 'Soals',
                meta: {requiredAuth: true},
                component: () => import("@/views/SoalPage.vue"),
            },
            {
                path: '/class-detail/:id',
                name: 'ClassDetail',
                meta: {requiredAuth: true},
                component: () => import("@/views/KelasDetailPage.vue"),
            },
        ]
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to: Object, from: Object, next: Function) => {
    const requiredAuth: any = to["matched"].some((record: Object) => record["meta"].requiredAuth);
    const deniedForAuth: any = to["matched"].some((record: Object) => record["meta"].deniedForAuth);

    if (requiredAuth && !Authentication.isLogin()) {
        return next({path: Authentication.defaultPath});
    }

    if (Authentication.isLogin() && deniedForAuth) {
        return next({path: Authentication.homePath});
    }

    return next();
});

export default router
