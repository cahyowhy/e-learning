import {Vue} from "vue-property-decorator";
import { Deserialize } from 'cerialize';
import {Inject} from "typescript-ioc";
import User from "@/models/User";
import StorageService from "@/service/StorageService";

export default class Authentication {

    @Inject
    private static storageService: StorageService;

    public static defaultPath: string = "/";

    public static homePath: string = "/home";

    /**
     * Method isLogin to get login state
     * user must be object to valid it is login
     *
     * @returns {boolean} user has login or not
     */
    public static isLogin() {
        const user: any = Authentication.getUser();

        return !(user === 'undefined' || typeof user === 'undefined' || typeof user === 'string') &&
            typeof user === 'object' && Object.keys(user).length !== 0 && typeof user.token === 'string';
    }

    /**
     * Method getUser to get user from session
     *
     * @param param {string} key of user (ex: username)
     * @param defaultIsObject is boolean default return
     *        empty object or pure value (to prevent error)
     *        please set true on nested object
     * @return undefine or empty object when defaultIsObject
     */
    public static getUser(param: string = '', defaultIsObject = false) {
        let user: any = Vue.prototype.$cookie.get('user');

        if (user && Object.keys(user).length && param !== '') {
            user = user[param];
        }

        return defaultIsObject ? user || {} : user;
    }

    /**
     * Method setUser to save user in session and redirect path
     *
     * @param param object user
     * @param isUpdate boolean set user method is update or new
     */
    public static setUser(param: any, isUpdate: boolean = false) {
        if (isUpdate) {
            // if on update, merging old user with new user
            delete param.token; // remove token

            param = Object.assign(Authentication.getUser(), param);
        }

        Vue.prototype.$cookie.set('user', Deserialize(param, User));

        if (!isUpdate && this.isLogin()) {
            window.location.href = '/home';
        }
    }

    /**
     * Method removeUser to delete user in cookies
     * dependence delete all cookies too
     * and then redirect to root path
     *
     * @param allData {boolean} is remove all local storage too?
     */
    public static removeUser(allData: boolean = true) {
        // remove all cookie and all storage
        Vue.prototype.$cookie.remove();

        if (allData) {
            Authentication.storageService.remove();
        }

        window.location.href = '/';
    }
}
