/**
 * Created by cahyo on 03/12/2018.
 *
 * File environment for development
 * this file can import by alias 'environment'
 */

import BaseEnvironment from './BaseEnvironment';

export default ((ENV) => {

    ENV['LOGGER'] = true;
    ENV['NODE_ENV'] = process.env.NODE_ENV;
    ENV['BASE_API'] = process.env.RESOURCE["BASE_API"];
    ENV['API_URL'] = process.env.RESOURCE["BASE_API"];
    ENV['API_USER'] = ENV['BASE_API'] + 'users/';
    ENV['API_ROLE'] = ENV['BASE_API'] + 'roles/';
    ENV['API_INFORMASI'] = ENV['BASE_API'] + 'informasis/';
    ENV['API_MAPEL'] = ENV['BASE_API'] + 'mapels/';
    ENV['API_KELAS'] = ENV['BASE_API'] + 'kelas/';
    ENV['API_SOAL'] = ENV['BASE_API'] + 'soals/';

    return ENV;
})(BaseEnvironment);
