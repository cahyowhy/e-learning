import {Vue} from 'vue-property-decorator';
import router from "./router";
import Constant from "@/config/Constant";

import './plugin/VueCookie';
import './plugin/VueOther';

import i18n from './plugin/Vuei18n';
import "./style/app.scss";

import CommonImage from "@/components/CommonImage.vue";
import EmptyStates from "@/components/EmptyStates.vue";
import NavigationBar from "@/components/NavigationBar.vue";
import UserBadge from "@/components/UserBadge.vue";
import CommonTable from "@/components/CommonTable.vue";
import InformasiItem from "@/components/InformasiItem.vue";
import MasterMapel from "@/components/MasterMapel.vue";
import FormMapel from "@/components/FormMapel.vue";
import MapelItem from "@/components/MapelItem.vue";
import FormKelas from "@/components/FormKelas.vue";
import ImageUpload from "@/components/ImageUpload.vue";
import KelasItem from "@/components/KelasItem.vue";
import FormSoal from "@/components/FormSoal.vue";

export default class AppConfig {

    private static vm: any = null;

    public static registerDefaultComponent() {
        Vue.component("common-image", CommonImage);
        Vue.component("empty-states", EmptyStates);
        Vue.component("navigation-bar", NavigationBar);
        Vue.component("user-badge", UserBadge);
        Vue.component("common-table", CommonTable);
        Vue.component("informasi-item", InformasiItem);
        Vue.component("master-mapel", MasterMapel);
        Vue.component("form-mapel", FormMapel);
        Vue.component("mapel-item", MapelItem);
        Vue.component("form-kelas", FormKelas);
        Vue.component("image-upload", ImageUpload);
        Vue.component("kelas-item", KelasItem);
        Vue.component("form-soal", FormSoal);
    }

    public static getVm() {
        if (AppConfig.vm === null) {
            let el = '#app';

            AppConfig.vm = new Vue({
                el, i18n, router,
            });
        }

        return AppConfig.vm;
    }

    public static setVueConfig() {
        Vue.config.productionTip = false;
        Vue.config.devtools = false;
        Vue.config.silent = true;

        Vue.prototype.Constant = Constant;
    }

    public static setFilter() {
        const filters: any = require.context("./util", true, /\.(ts)$/i);

        filters.keys().map((key: any) => {
            let filterName: any = key.match(/\w+/)[0];

            if (filterName !== "Annotation") {
                const filter: any = filters(key);

                Object.keys(filter).map(exportKey => {
                    Vue.filter(exportKey, filter[exportKey]);
                });
            }
        });
    }

    public static init() {
        AppConfig.registerDefaultComponent();
        AppConfig.setVueConfig();
        AppConfig.setFilter();

        AppConfig.getVm().$mount("#app");
    }

    /**
     * Method showNotification base on available
     * constant response text from translation,
     * default is message network problem with code '0000'
     *
     * @param params {object|any} response API / {string} if custom true
     * @param custom {boolean} boolean if is custom notification
     *        will direct passing param as notifcation text, not from translation
     * @param error {boolean} is notification for error?
     * @param paramOption
     */
    public static showNotification(params: any, custom = false, error: boolean = false, paramOption: any = {}) {
        try {
            if (this.vm && this.vm.$snackbar !== undefined && params) {
                const status: any = params.status !== undefined ? params.status : '0000';
                const options: any = {
                    duration: 3000,
                    queue: false,
                    message: custom ? params : this.vm.$t(`notification.${status}`),
                    type: `is-${error ? 'danger' : custom ? 'info' : 'warning'}`,
                    position: 'is-top-right'
                };
                Object.assign(options, paramOption);

                this.vm.$snackbar.open(options);
            }
        } catch (e) {
            console.log(e);
        }
    }
}
