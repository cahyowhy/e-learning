/**
 * Created by cahyo on 01/22/2018.
 */

import {isEmpty} from 'lodash';

export const getBlobFile = (dataURI: string, mimetype: string = '') => {
    let data = dataURI.split(',');
    let mimetypeFile = !isEmpty(mimetype) ? mimetype : data[0].match(/:(.*?);/)[1];
    let bytestring = atob(data[1]);
    let index = bytestring.length;
    let blobArray = new (<any>window).Uint8Array(index);

    while (index--) {
        blobArray[index] = bytestring.charCodeAt(index);
    }

    return new Blob([blobArray], {type: mimetypeFile});
};

export const fileToDataURL = (file: any): Promise<any> => {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();
        reader.addEventListener('load', function () {
            resolve(reader.result);
        }, false);
        reader.addEventListener('error', function (e) {
            reject(e.toString());
        });

        reader.readAsDataURL(file);
    });
};

/**
 * from file image to file image resizing
 *
 * @param {File} fileImage
 * @param imageSize
 * @param asFile
 * @param quality
 * @param filename
 * @returns {Promise<any>}
 */
export const imageResize = (fileImage: File, imageSize: number = 720,
                            asFile: boolean = true,
                            quality: number = 1.0,
                            filename: string = "") => {
    let formatFiles = fileImage.name.split('.');
    let formatFile: string = formatFiles[formatFiles.length - 1];
    formatFile = formatFile === 'png' ? 'png' : 'jpeg';

    return new Promise((resolve, reject) => {
        const canvas = document.createElement('canvas');
        const image = new Image();

        fileToDataURL(fileImage).then((response: any) => {
            image.src = response;

            image.onload = function () {
                if ((<any>this).naturalWidth > (<any>this).naturalHeight) {
                    canvas.height = (<any>this).naturalHeight * (imageSize / (<any>this).naturalWidth);
                    canvas.width = imageSize;
                } else if (image.naturalWidth < image.naturalHeight) {
                    canvas.width = (<any>this).naturalWidth * (imageSize / (<any>this).naturalHeight);
                    canvas.height = imageSize;
                } else {
                    canvas.width = (<any>this).naturalWidth;
                    canvas.height = (<any>this).naturalHeight;
                }

                canvas.getContext('2d').drawImage(image, 0, 0, (<any>this).naturalWidth, (<any>this).naturalHeight,
                    0, 0, canvas.width, canvas.height);
                const src = canvas.toDataURL('image/' + formatFile, quality);

                if (asFile) {
                    resolve(
                        new File([getBlobFile(src)],
                            (filename.toString() === "") ? ('image.' + formatFile) : filename)
                    );
                } else {
                    resolve(src);
                }
            };
            image.onerror = function () {
                reject('gagal muat gambar');
            };
        });
    });
};