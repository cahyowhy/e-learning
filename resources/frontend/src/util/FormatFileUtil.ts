/**
 * Created by cahyo on 09/05/18.
 */

export const validDocFiles = [
    '.pdf',
    '.doc',
    '.docx',
    '.ppt',
    '.pptx',
    '.xls',
    '.xlsx',
];

export const validImageFiles = [
    '.jpg',
    '.jpeg',
    '.gif',
    '.png',
];

export const validFormatFiles = [
    '.jpg',
    '.jpeg',
    '.gif',
    '.png',
    '.svg',
    '.pdf',
    '.doc',
    '.docx',
    '.ppt',
    '.pptx',
    '.xls',
    '.xlsx',
    '.mp3',
    '.ogg',
    '.amr',
    '.m4a',
    '.wav',
    '.aac',
    '.zip',
    '.rar'
];

export const reducer = (arr: Array<any>) => {
    return arr.reduce(function (accu, item, index) {
        return index === 0 ? accu + item : accu + ", " + item
    }, '');
};

export const acceptFormatFiles = reducer(validFormatFiles);

export const acceptImageFiles = reducer(validImageFiles);

export const acceptDocFiles = reducer(validDocFiles);

export const getMimeType = (formatFile: string = 'jpg'): string => {
    let validImages = validImageFiles.reduce((accu: any, item: string) =>
        accu || item === `.${formatFile}`, false);

    if (validImages) {
        return 'image/' + formatFile;
    }

    return 'application/' + formatFile;
};
