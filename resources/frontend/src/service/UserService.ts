/**
 * Created by cahyo on 06/20/2018.
 *
 * UserService class for provide User rest
 * with singleton concept provided by decorator
 */

import {Singleton} from 'typescript-ioc';
import Resource from '../config/environment/Resource';
import {isNil} from 'lodash';
import ProxyService from './Proxy';
import {Serialize, Deserialize} from 'cerialize';

import EntityAware from '../models/EntityAware';
import User from '../models/User';

@Singleton
export default class UserService extends ProxyService {

    public api: string = Resource['API_USER']; // define api url to fetch data

    public serializer: EntityAware = User;     // define serializer after fetch data

    public doLogin(T: EntityAware): Promise<any> {
        this.redirectOnFailedAuth = false;
        const api = Resource['API_URL'] + 'login';

        return this.post(T, api).then((response: any = {}) => {
            if ((response || {}).hasOwnProperty("data")) {
                this.commonService.setUser(response.data);
            }

            return response;
        });
    }

    public update(T: EntityAware): Promise<any> {
        return super.update(T).then((response) => {
            if (typeof response === 'object') {
                if (this.returnWithStatus && response.data) {
                    response.data = Deserialize(response.data, User);
                } else if (!this.returnWithStatus) {
                    response = Deserialize(response.data, User);
                }
            }

            return response;
        });
    }

    public updateUserPassword(T: EntityAware): Promise<any> {
        return this.post(Serialize(T), this.api + 'update-password');
    }

    public uploadImageProfile(entity: User): Promise<any> {
        const api = this.api + 'upload-image-profile/' + this.commonService.getUser('id');

        const param: FormData = new FormData();
        const file = (<any>entity).file;

        if (!isNil(file)) {
            param.append('file', file);
        }

        return this.post(param, api, {'Content-Type': 'multipart/form-data'})
        .then((response: any) => this.convertResponse(response, this.returnWithStatus));
    }
}
