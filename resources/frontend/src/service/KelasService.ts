/**
 * Created by cahyo on 06/20/2018.
 *
 * KelasService class for provide User rest
 * with singleton concept provided by decorator
 */

import {Singleton} from 'typescript-ioc';
import Resource from '../config/environment/Resource';

import ProxyService from './Proxy';

import EntityAware from '../models/EntityAware';
import Kelas from '../models/Kelas';

@Singleton
export default class KelasService extends ProxyService {

    public api: string = Resource['API_KELAS']; // define api url to fetch data

    public serializer: EntityAware = Kelas;

    public checkSubscribeKelas(param: any): Promise<boolean> {
        const api = this.api + 'check-kelas-subscribe';

        return super.get(param, api).then(response => response.data || false);
    }

    public subscribeKelas(param: any): Promise<any> {
        const api = this.api + 'subscribe';

        return super.post(param, api);
    }

    public activateKelas(param: any): Promise<any> {
        const api = this.api + 'activate';

        return super.get(param, api);
    }
}
