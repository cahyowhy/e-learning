/**
 * Created by cahyo on 06/20/2018.
 *
 * InformasiService class for provide User rest
 * with singleton concept provided by decorator
 */

import { Singleton } from 'typescript-ioc';
import Resource from '../config/environment/Resource';

import ProxyService from './Proxy';

import EntityAware from '../models/EntityAware';
import Informasi from '../models/Informasi';

@Singleton
export default class InformasiService extends ProxyService {

  public api: string = Resource['API_INFORMASI']; // define api url to fetch data

  public serializer: EntityAware = Informasi;
}
