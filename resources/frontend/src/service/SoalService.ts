/**
 * Created by cahyo on 06/20/2018.
 *
 * SoalService class for provide Soal rest
 * with singleton concept provided by decorator
 */

import {Singleton} from 'typescript-ioc';
import Resource from '../config/environment/Resource';
import {Serialize} from 'cerialize';
import {isNil} from 'lodash';

import ProxyService from './Proxy';

import EntityAware from '../models/EntityAware';
import Soal from '../models/Soal';
import MahasiswaSoal from '../models/MahasiswaSoal';

@Singleton
export default class SoalService extends ProxyService {

    public api: string = Resource['API_SOAL']; // define api url to fetch data

    public serializer: EntityAware = Soal;

    public findMinMaxSoal(param: any): Promise<any> {
        const api = this.api + 'min-maxs';

        return super.get(param, api).then((response) => response.data);
    }

    public find(param: any): Promise<any> {
        let serializer = param.hasOwnProperty('id') ||
        param.hasOwnProperty('mahasiswa_id') ? MahasiswaSoal : Soal;

        return this.get(param).then((response: any) =>
            this.convertResponse(response, false, serializer));
    }

    public save(entity: EntityAware): Promise<any> {
        const param: FormData = new FormData();
        let paramJson: any = Serialize(entity, Soal);
        const files = (<any>entity).files;

        if (!isNil(files) && Array.isArray(files)) {
            files.forEach((item, index) => {
                param.append(`file-${index}`, item);
            });
        }


        param.append('soal', JSON.stringify(paramJson));

        return this.post(param, this.api, {'Content-Type': 'multipart/form-data'})
            .then((response: any) => this.convertResponse(response, this.returnWithStatus));
    }

    public updateMahasiswaSoal(entity: EntityAware): Promise<any> {
        const param: FormData = new FormData();
        const file = (<any>entity).file;
        let paramJson: any = Serialize(entity, MahasiswaSoal);

        param.append(`file`, file);
        param.append('mahasiswa_soal', JSON.stringify(paramJson));

        return this.post(param, this.api + 'update-mahasiswa-soals', {'Content-Type': 'multipart/form-data'})
            .then((response: any) => this.convertResponse(response, this.returnWithStatus));
    }
}
