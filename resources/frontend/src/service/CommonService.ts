/**
 * Created by fajar on 01/23/2018.
 *
 * CommonService is service which help anywhere
 */

import {Singleton} from 'typescript-ioc';

import Authentication from "@/config/Authentication";
import AppConfig from "@/app";

@Singleton
export default class CommonService {

	public showNotification(params: any, custom = false, error: boolean = false, paramOption: any = {}) {
		AppConfig.showNotification(params, custom, paramOption)
	}

	public isLogin() {
		return Authentication.isLogin();
	}

	public setUser(param: any, isUpdate: boolean = false) {
		Authentication.setUser(param, isUpdate)
	}

	public getUser(param: string = '', defaultIsObject = false) {
		return Authentication.getUser(param, defaultIsObject)
	}

	public removeUser(allData: boolean = true) {
		Authentication.removeUser(allData);
	}
}
