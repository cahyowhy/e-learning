/**
 * Created by cahyo on 06/20/2018.
 *
 * UserService class for provide User rest
 * with singleton concept provided by decorator
 */

import {Singleton} from 'typescript-ioc';
import Resource from '../config/environment/Resource';
import {isNil} from 'lodash';
import {Serialize} from 'cerialize';

import ProxyService from './Proxy';

import EntityAware from '../models/EntityAware';
import Mapel from '../models/Mapel';

@Singleton
export default class MapelService extends ProxyService {

    public api: string = Resource['API_MAPEL']; // define api url to fetch data

    public serializer: EntityAware = Mapel;

    public save(entity: Mapel): Promise<any> {
        const param: FormData = new FormData();
        let paramJson: any = Serialize(entity, Mapel);
        const file = (<any>entity).file;

        if (!isNil(file)) {
            param.append('file', file);
        }


        param.append('mapel', JSON.stringify(paramJson));

        return this.post(param, this.api, {'Content-Type': 'multipart/form-data'})
            .then((response: any) => this.convertResponse(response, this.returnWithStatus));
    }
}
