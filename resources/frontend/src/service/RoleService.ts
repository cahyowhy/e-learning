/**
 * Created by cahyo on 06/20/2018.
 *
 * UserService class for provide User rest
 * with singleton concept provided by decorator
 */

import {Singleton} from "typescript-ioc";
import Resource from '../config/environment/Resource';

import ProxyService from './Proxy';

import EntityAware from '../models/EntityAware';
import Role from '../models/Role';

@Singleton
export default class RoleService extends ProxyService {

  public api: string = Resource['API_ROLE']; // define api url to fetch data

  public serializer: EntityAware = Role;
}
