import {deserialize, serialize, inheritSerialization, deserializeAs} from "cerialize";
import {normalizeUnderscore} from "../util/StringUtil";
import {cloneDeep, isEmpty} from "lodash";
import moment from 'moment';
import Constant from '../config/Constant';

import TableColumn from "./TableColumn";
import Base from "./Base";
import Kelas from "./Kelas";

@inheritSerialization(Base)
export default class Soal extends Base {
    @serialize
    @deserialize
    public name: string = "";

    @deserializeAs(Kelas)
    public kelas: Kelas = new Kelas();

    @deserialize
    public published: boolean = false;

    @serialize
    @deserialize
    public description: string = "";

    @serialize
    @deserialize
    public kelas_id: number = 0;

    @serialize
    @deserialize
    public due: Date = new Date();

    public files: Array<any> = [];

    public nameFeedback(): any {
        const valid = this.name.length > 9 && this.name.length <= 25;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Minimal 10 karakter, Maksimal 25 karakter";

        return {
            valid,
            type,
            error
        };
    }

    public descriptionFeedback(): any {
        const valid = this.description.length > 29;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Minimal 30 karakter";

        return {
            valid,
            type,
            error
        };
    }

    public table(): any {
        const {id, name, kelas, due} = this;

        return {id, name, kelas: kelas.name, batas_waktu: moment(due).format(Constant.DATE_PATTERN)};
    }

    public soalFileData(): Array<any> {
        return cloneDeep(this.files).map(item => {
            return {
                nama: item.name,
                size: item.size / 1000 + ' kB'
            }
        });
    }

    public soalFileColumn(): Array<TableColumn> {
        return ['no', 'nama', 'size', 'aksi'].map(field => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "no";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";

            return tableColumn;
        });
    }

    public soalValid(minMax: any): boolean {
        const {files} = this;
        const validProp = this.descriptionFeedback().valid && this.nameFeedback().valid;

        return !isEmpty(minMax) ? (validProp && files.length >= minMax.min && files.length <= minMax.max)
            : validProp;
    }

    public static columnName(): Array<TableColumn> {
        const soal = new Soal().table();
        let keys = Object.keys(soal);
        keys.push("aksi");
        keys.unshift("no");

        return keys.map((field: any, index: number) => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "aksi" || field === "no";
            tableColumn.width = "auto";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";
            tableColumn.visible = field !== "id";

            return tableColumn;
        });
    }

    public static OnSerialized(instance: Soal, json: any): void {
        if (parseInt(json.id) === 0) {
            delete json.id;
        }

        json.due = moment(instance.due).format(Constant.DATE_TIMESTAMP).toString();
    }

    public static OnDeserialized(instance: Soal, json: any): void {
        const createdDate = json.created_at || new Date().toDateString();
        instance.created_at = moment(new Date(createdDate)).format(Constant.DATE_PATTERN);
        instance.due = json.due ? new Date(json.due) : new Date();
    }
}
