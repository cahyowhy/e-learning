import {deserialize, serialize, inheritSerialization, deserializeAs} from "cerialize";
import {normalizeUnderscore} from "@/util/StringUtil";
import moment from "moment";

import TableColumn from "./TableColumn";
import Base from "./Base";
import Soal from "./Soal";
import User from "./User";
import Constant from "../config/Constant";

@inheritSerialization(Base)
export default class MahasiswaSoal extends Base {

    @deserialize
    public kode: string = "";

    @serialize
    public download_path: string = "";

    @deserializeAs(Soal)
    public soal: Soal = new Soal();

    @deserializeAs(User)
    public mahasiswa: User = new User();

    @deserialize
    public soal_id: number = 0;

    @deserialize
    public mahasiswa_id: number = 0;

    public file: any = null;

    public table(): any {
        const {id, soal: {name, due}, mahasiswa, download_path} = this;

        return {
            id, soal: name, download_path,
            batas_waktu: moment(due).format(Constant.DATE_PATTERN),
            mahasiswa: mahasiswa.name
        };
    }

    public valid(): boolean {
        return this.id > 0 && this.file instanceof File;
    }

    public static columnName(): Array<TableColumn> {
        const soal = new MahasiswaSoal().table();
        let keys = Object.keys(soal);
        keys.push("aksi");
        keys.unshift("no");

        return keys.map((field: any, index: number) => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "aksi" || field === "no";
            tableColumn.visible = field !== "id" && field !== "download_path";
            tableColumn.width = "auto";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";

            return tableColumn;
        });
    }

    public static OnDeserialized(instance: MahasiswaSoal, json: any): void {
        if (json.download_path) {
            instance.download_path = '/user-files/soals/' + json.download_path;
        }
    }
}
