import {deserialize, serialize, inheritSerialization} from "cerialize";
import Base from "./Base";

@inheritSerialization(Base)
export default class Role extends Base {

    @serialize
    @deserialize
    public name: string = "";
}
