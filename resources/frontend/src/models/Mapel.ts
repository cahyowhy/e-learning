import {deserialize, serialize, inheritSerialization} from "cerialize";
import TableColumn from "./TableColumn";
import {normalizeUnderscore} from "../util/StringUtil";
import Base from "./Base";
import moment from 'moment';
import Constant from "../config/Constant";

@inheritSerialization(Base)
export default class Mapel extends Base {
    @serialize
    @deserialize
    public name: string = "";

    @deserialize
    public image: string = "";

    public file: any = null;

    public nameFeedback(): any {
        const valid = this.name.length > 4 && this.name.length <= 25;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Minimal 5 karakter, Maksimal 25 karakter";

        return {
            valid,
            type,
            error
        };
    }

    public table(): any {
        const {id, name} = this;

        return {id, name};
    }

    public static columnName(): Array<TableColumn> {
        const mapel = new Mapel().table();
        let keys = Object.keys(mapel);
        keys.push("aksi");
        keys.unshift("no");

        return keys.map((field: any, index: number) => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "no";
            tableColumn.width = "auto";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";
            tableColumn.visible = field !== 'id';

            return tableColumn;
        });
    }

    public static OnDeserialized(instance: Mapel, json: any): void {
        const createdDate = json.created_at || new Date().toDateString();
        instance.created_at = moment(new Date(createdDate)).format(Constant.DATE_PATTERN);

        if (json.image) {
            const imageName = '/user-files/mapels/' + json.image;

            instance.image = imageName;
            instance.file = {name: imageName};
        } else {
            instance.image = '/images/education.jpeg';
        }
    }
}
