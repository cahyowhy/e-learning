import {inheritSerialization, deserialize, serialize, deserializeAs} from "cerialize";
import {normalizeUnderscore} from "@/util/StringUtil";
import Base from "./Base";
import User from "./User";
import Mapel from "./Mapel";
import moment from 'moment';

import TableColumn from "./TableColumn";
import Constant from "../config/Constant";

@inheritSerialization(Base)
export default class Kelas extends Base {
    @serialize
    @deserialize
    public name: string = "";

    @serialize
    @deserialize
    public max: number = 5;

    @serialize
    @deserializeAs(User)
    public dosen: User = new User();

    @serialize
    @deserializeAs(Mapel)
    public mapel: Mapel = new Mapel();

    @serialize
    @deserialize
    public dosen_id: number = 0;

    @serialize
    @deserialize
    public mapel_id: number = 0;

    @deserialize
    public total_subscribe: number = 0;

    @deserialize
    public active: boolean = false;

    public nameFeedback(): any {
        const valid = this.name.length > 5 && this.name.length <= 15;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Minimal 5 karakter, Maksimal 15 karakter";

        return {
            valid,
            type,
            error
        };
    }

    public dosenIdFeedback(): any {
        const valid = this.dosen_id !== 0 && typeof this.dosen_id === "number";
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Tidak boleh kosong";

        return {
            type,
            valid,
            error
        };
    }

    public mapelIdFeedback(): any {
        const valid = this.mapel_id !== 0 && typeof this.mapel_id === "number";
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Tidak boleh kosong";

        return {
            type,
            valid,
            error
        };
    }

    public table(): any {
        const {id, name, dosen, mapel, max, created_at} = this;

        return {id, name, dosen: dosen.name, mata_pelajaran: mapel.name, max, created_at};
    }

    public valid(): boolean {
        return (
            this.nameFeedback().valid &&
            this.dosenIdFeedback().valid &&
            this.mapelIdFeedback().valid &&
            (this.max >= 5 || this.max <= 10)
        );
    }

    public static columnName(): Array<TableColumn> {
        const kelas = new Kelas().table();
        let keys = Object.keys(kelas);
        keys.push("aksi");
        keys.unshift("no");

        return keys.map((field: any, index: number) => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "no" || field === "created_at";
            tableColumn.width = "auto";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";
            tableColumn.visible = field !== 'id';

            return tableColumn;
        });
    }

    public static OnDeserialized(instance: Kelas, json: any): void {
        const createdDate = json.created_at || new Date().toDateString();
        instance.created_at = moment(new Date(createdDate)).format(Constant.DATE_PATTERN);

        instance.active = json.active === 1;
    }
}
