import moment from "moment";
import Constant from "../config/Constant";
import {deserialize, inheritSerialization, serialize, deserializeAs} from "cerialize";
import {normalizeUnderscore} from "@/util/StringUtil";

import Base from "./Base";
import Role from "./Role";
import TableColumn from "./TableColumn";

@inheritSerialization(Base)
export default class User extends Base {
    @deserialize
    @serialize
    public name: string = "";

    @deserialize
    public image_profile: string = "";

    @deserialize
    @serialize
    public email: string = "";

    @deserialize
    @serialize
    public address: string = "";

    @deserialize
    @serialize
    public phone_number: string = "";

    @serialize
    public password: string = "";

    @serialize
    public passwordOld: string = "";

    @deserialize
    public token: string = "";

    @serialize
    @deserialize
    public role_id: number = 0;

    @deserialize
    public password_confirmation: string = "";

    @deserializeAs(Role)
    public role: Role = new Role();

    public file: any = null;

    public nameFeedback(): any {
        const valid = this.name.length > 4 && this.name.length <= 50;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Minimal 5 karakter, Maksimal 50 karakter";

        return {
            valid,
            type,
            error
        };
    }

    public passwordFeedback(): any {
        const valid = this.password.length > 5;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Password, minimal 6 karakter";

        return {
            type,
            valid,
            error
        };
    }

    public roleIdFeedback(): any {
        const valid = this.role_id !== 0 && typeof this.role_id === "number";
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Tidak boleh kosong";

        return {
            type,
            valid,
            error
        };
    }

    public table(): any {
        const {id, email, name, role, created_at} = this;

        return {id, email, name, role: role.name, created_at};
    }

    public passwordOldFeedback(): any {
        const valid = this.passwordOld.length !== 0;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Password lama tidak boleh kosong";

        return {
            type,
            valid,
            error
        };
    }

    public passwordConfirmFeedback(): any {
        const valid = this.password === this.password_confirmation;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Password tidak sama";

        return {
            type,
            valid,
            error
        };
    }

    public emailFeedback(): any {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const valid = re.test(String(this.email).toLowerCase());
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Format email salah";

        return {
            valid,
            type,
            error
        };
    }

    public validLogin(): boolean {
        return this.emailFeedback().valid && this.passwordFeedback().valid;
    }

    public validUpdatePassword(): boolean {
        return (
            this.passwordOldFeedback().valid &&
            this.passwordConfirmFeedback().valid &&
            this.passwordFeedback().valid
        );
    }

    public valid(): boolean {
        return (
            this.nameFeedback().valid && this.emailFeedback().valid && this.roleIdFeedback().valid
        );
    }

    public validRegister(): boolean {
        return (
            this.emailFeedback().valid &&
            this.nameFeedback().valid &&
            this.passwordFeedback().valid &&
            this.passwordConfirmFeedback().valid &&
            this.roleIdFeedback().valid
        );
    }

    public loginProperty(): any {
        const {email, password} = this;

        return {email, password};
    }

    public static OnDeserialized(instance: User, json: any): void {
        const createdDate = json.created_at || new Date().toDateString();
        instance.created_at = moment(new Date(createdDate)).format(Constant.DATE_PATTERN);

        if (json.image_profile) {
            instance.image_profile = '/user-files/users/' + json.image_profile;
        } else if (json.role_id) {
            if (json.role_id === 1) {
                instance.image_profile = "/images/admin-avatar.svg";
            } else if (json.role_id === 2) {
                instance.image_profile = "/images/dosen-avatar.svg";
            } else if (json.role_id === 3) {
                instance.image_profile = "/images/student-avatar.svg";
            } else {
                instance.image_profile = "/images/default-user.jpg";
            }
        }
    }

    public static OnSerialized(instance: User, json: any): void {
        if (parseInt(json.id) === 0) {
            delete json.id;
        }

        if (parseInt(json.role_id) === 0) {
            delete json.role_id;
        }

        if (!json.password) {
            delete json.password;
        }
    }

    public static columnName(skip_action: boolean = false): Array<TableColumn> {
        const user = new User().table();
        let keys = Object.keys(user);

        if (!skip_action) keys.push("aksi");
        keys.unshift("no");

        return keys.map((field: any, index: number) => {
            const tableColumn = new TableColumn();

            tableColumn.field = field;
            tableColumn.label = normalizeUnderscore(field);
            tableColumn.centered = field === "created_at" || field === 'no';
            tableColumn.width = "auto";
            tableColumn.customSlot = field === "aksi";
            tableColumn.isNumbering = field === "no";
            tableColumn.visible = field !== 'id';

            return tableColumn;
        });
    }
}
