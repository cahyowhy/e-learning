import { inheritSerialization, deserialize, serialize } from "cerialize";
import Base from "./Base";
import moment from "moment";
import Constant from "../config/Constant";
import { htmlTagToText } from "../util/StringUtil";

@inheritSerialization(Base)
export default class Informasi extends Base {
    @serialize
    @deserialize
    public name: string = "";

    @serialize
    @deserialize
    public description: string = "";

    @serialize
    public subdescription: string = "";

    @serialize
    @deserialize
    public published: boolean = true;

    public nameFeedback(): any {
        const valid = this.name.length > 10;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Name must be at least 10 char";

        return {
            valid,
            type,
            error
        };
    }

    public descriptionFeedback(): any {
        const valid = this.description.length > 50;
        const type = `is-${valid ? "success" : "danger"}`;
        const error = valid ? "" : "Description must not be empty";

        return {
            valid,
            type,
            error
        };
    }

    public valid(): boolean {
        return this.nameFeedback().valid && this.descriptionFeedback().valid;
    }

    public static OnDeserialized(instance: Informasi, json: any): void {
        const createdDate = json.created_at || new Date().toDateString();
        instance.created_at = moment(new Date(createdDate)).format(Constant.DATE_PATTERN);
        instance.subdescription = htmlTagToText(json.description);

        instance.published = json.published === 1;
    }

    public static OnSerialized(instance: Informasi, json: any): void {
        if (parseInt(json.id) === 0) {
            delete json.id;
        }

        json.published = json.published ? 1 : 0;
    }
}
