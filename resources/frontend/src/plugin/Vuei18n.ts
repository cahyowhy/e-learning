import {Vue} from 'vue-property-decorator';
import VueI18n from 'vue-i18n';

import Resource from '../config/environment/Resource';
import indonesia from '../locale/indonesia';

Vue.use(VueI18n);

export default new VueI18n({
  locale: Resource.LOCALE,
  fallbackLocale: 'id',
  messages: {
    'id': indonesia
  }
});
