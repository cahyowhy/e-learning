declare module "*.vue" {
    import Vue = require("vue");

    // @ts-ignore
    const value: Vue.ComponentOptions<Vue>;
    export default value;
}


declare module 'vue-i18n';
declare module 'environment';
declare module 'query-string';

declare module 'js-cookie';
declare module 'base-64';
declare module 'buefy';
declare module 'crypto-js';
declare module 'v-img-fallback';
declare module 'secure-ls';
declare module 'moment';
declare module 'vue2-editor';
