## How to run project
### Run web service
1. install the dependencies first, use `composer i` on main dir (if not installed yet)
2. create `.env` file from main dir.  You can copy from `.env.example`. (if not created yet)
3. run your mysql local environment. Create the database, make sure the value its from `DB_DATABASE` key from your `.env` file (if not filled yet)
4. Fill all the DB_ prefix key value with your local environment (if not filled yet)
5. run your mysql local environment
6. run `php artisan migrate` to create migration
6. run `php artisan key:generate` to generate uniqe key
6. run `php artisan jwt:secret` to generate jwt secret
7. run `php artisan db:seed` to seed the user data on mysql
8. do `build project` on frontend service from step 1 & 3 bellow on how to `Run frontend service`
9. run `php -S localhost:8000 -t public` on your terminal to run the web

### Run only frontend service
1. install the dependencies first, use `npm i` on main dir (if not installed yet)
2. `npm run serve` to run web on development
3. `npm run build` to build project
4. `npm run test:unit` to do test unit on frontend


### NOTE
- jika ingin mengedit file yang ada di `/resources/frontend` dan ingin perubahan yang disimpan disitu langsung ter 
Apply di browser gunakan perintah `php artisan serve` dan `npm run serve`. Kelebihan dari perintah ini adalah setiap 
ada perubahan di folder tsb akan langsung ter refresh di browser, kelemahanya menghabiskan memory. 
Tampilan web ada di halaman http://127.0.0.1:8080/ 
- jika tidak ingin menggunakan live reload, maka gunakan perintah `php artisan serve` dan `npm run build`. 
Script `npm run build` digunakan untuk membuild ui yang ada di folder `/resources/frontend`. Tampilan web ada di halaman http://127.0.0.1:8000/ 
