<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $fillable = [
        'name', 'description', 'published', 'kelas_id', 'due'
    ];

    public function mahasiswa_soals()
    {
        return $this->hasMany('App\MahasiswaSoal');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Kelas');
    }
}
