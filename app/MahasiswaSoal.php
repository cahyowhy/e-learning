<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaSoal extends Model
{
    protected $fillable = [
        'kode', 'mahasiswa_id', 'soal_id', 'download_path'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo('App\User', 'mahasiswa_id');
    }

    public function soal()
    {
        return $this->belongsTo('App\Soal');
    }
}
