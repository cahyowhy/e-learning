<?php
/**
 * Created by PhpStorm.
 * User: cahyo
 * Date: 11/15/18
 * Time: 11:21 AM
 */

namespace App\Util;

class LcgDistribution
{
    public static function fpengali(int $n)
    {
        $fps = [];

        for ($x = 1; $x < $n; $x++) {
            if ($n % $x === 0) {
                array_push($fps, $x);
            }
        }

        return $fps;
    }

    public static function is_prime(int $n)
    {
        for ($x = 2; $x < $n; $x++) {
            if ($n % $x == 0) {
                return false;
            }
        }

        return true;
    }

    public static function generate_multiplier($bilAsli, $x)
    {
        $c = $x;
        do {
            $c += 1;
        } while (!self::is_prime($c));

        $npengali = self::fpengali($bilAsli);

        for ($i = 0; $i < count($npengali); $i++) {
            if (($c - 1) % $npengali[$i] !== 0) {
                return self::generate_multiplier($bilAsli, $c + 1);
            }
        }

        return $c;
    }

    public static function lcg($seed, $modulo, $multiply, $increment)
    {
        return ($multiply * $seed + $increment) % $modulo;
    }

    public static function closest_prime($n)
    {
        for ($i = $n; $i > 0; $i--) {
            if (self::is_prime($i)) {
                return $i;
            }
        }

        return 1;
    }

    public static function generate_soals_distribution($murids, $soals, $soal_id)
    {
        $is_soals_even = count($soals) % 2 === 0;
        $ini_seed = 0;

        // agar dapat mendapatkan periode penuh. apabila total soal ganjil.
        // abaikan soal terakhir dalam proses distribusi
        // proses soal ini saat akhir
        $ini_modulo = $is_soals_even ? count($soals) : count($soals) - 1;
        $ini_multiply = self::generate_multiplier($ini_modulo, $ini_modulo);
        $ini_incr = self::closest_prime($ini_modulo);
        $results = [];
        $ini_seeds = [];

        foreach ($murids as $key => $value) {
            $ini_seed = self::lcg($ini_seed, $ini_modulo, $ini_multiply, $ini_incr);
            $kode = 'MS-' . $soal_id . '#M' . $value['mahasiswa_id'];
            $download_path = $soals[$ini_seed];

            // modif 2 nilai array terakhir dengan nilai dari soal terakhir
            // jika total soal ganjil
            if ($is_soals_even && $key > (count($murids) - 2)) {
                $download_path = $soals[count($soals) - 1];
            }

            $result = ['soal_id' => $soal_id,
                'mahasiswa_id' => $value['mahasiswa_id'],
                'kode' => $kode, 'download_path' => $download_path
            ];

            array_push($results, $result);
        }

        return $results;
    }
}