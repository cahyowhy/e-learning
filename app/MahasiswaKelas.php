<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaKelas extends Model
{
    protected $fillable = [
        'kode', 'mahasiswa_id', 'kelas_id', 'kode'
    ];

    public function kelas()
    {
        return $this->belongsTo('App\Kelas');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\User', 'mahasiswa_id');
    }
}
