<?php

namespace App\Http\Controllers;

use App\ResponseStatus;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Util\FileUpload;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Cookie;
use JWTAuth;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getDecodedTokenOrRedirect()
    {
        try {
            $token = JWTAuth::getToken();
            $payload = JWTAuth::getPayload()->toArray();

            return $payload;
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return [];
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return [];
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return [];
        }
    }

    public function generatePayloadFromRequest(Request $request, $valid_param)
    {
        $payload = [];
        foreach ($valid_param as $param) {
            if ($request->get($param) !== null) {
                $payload[$param] = $request->get($param);
            }
        }

        return $payload;
    }

    public function generateParamFromFormData(string $property, array $props)
    {
        $entity = json_decode($property, true);
        $result = [];

        foreach ($props as $key) {
            if (array_key_exists($key, $entity)) {
                $result[$key] = $entity[$key];
            }
        }

        return $result;
    }

    public function uploadFile(string $key, string $path, string $maxfilesizes = '2m', bool $image_only = false)
    {
        if (!is_dir(public_path('user-files'))) {
            mkdir(public_path('user-files'));
        }

        $fileUpload = new FileUpload();
        $dirpath = public_path('user-files/' . $path);
        $fileUpload->setInput($key);
        $filename = uniqid('userfile', true) . microtime(false) . ".%s";

        if ($image_only) {
            $fileUpload->setAllowMimeType('image');
        }

        if (!is_dir($dirpath)) {
            mkdir($dirpath);
        }

        $fileUpload->setMaxFileSize($maxfilesizes);
        $fileUpload->setDestinationDirectory($dirpath);
        $fileUpload->setFilename($filename);

        $fileUpload->save();

        return $fileUpload->getStatus() ? $fileUpload->getInfo()->filename : "";
    }

    public function unlink($filename)
    {
        // try to force symlinks
        if (is_link($filename)) {
            $sym = @readlink($filename);
            if ($sym) {
                return is_writable($filename) && @unlink($filename);
            }
        }

        // try to use real path
        if (realpath($filename) && realpath($filename) !== $filename) {
            return is_writable($filename) && @unlink(realpath($filename));
        }

        // default unlink
        return is_writable($filename) && @unlink($filename);
    }



    public function response_bad_request()
    {
        $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

        return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
    }
}
