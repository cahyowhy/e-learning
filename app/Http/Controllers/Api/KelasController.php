<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use App\Kelas;
use App\MahasiswaKelas;
use App\ResponseStatus;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offset = $request->query("offset");
        $limit = $request->query("limit");
        $mapel_id = $request->query("mapel");
        $mhs_id = $request->query("mhs_id");
        $kelas_id = $request->query("kelas_id");
        $dosen_id = $request->query("dosen_id");
        $all = $request->query("all");

        if (!isset($all)) {
            if (!isset($offset) || !isset($limit)) {
                $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

                return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
            }
        }

        $data = [];
        $count = 0;

        if (isset($mhs_id)) {
            $data = Kelas::whereHas('mahasiswa_kelas', function ($q) use ($mhs_id) {
                $q->where('mahasiswa_id', $mhs_id);
            })->with('dosen', 'mapel');

            if (isset($all)) {
                $data = $data->where('active', 1);
            }

            $count = $data->count();
        } else {
            $payload = [];
            $filters = ['mapel_id' => $mapel_id, 'id' => $kelas_id, 'dosen_id' => $dosen_id, 'active' => $all];
            foreach ($filters as $key => $value) {
                if (isset($value)) {
                    array_push($payload, $key === 'active' ? [$key, '=', 1] : [$key, '=', $value]);
                }
            }

            $data = Kelas::Join()->with('dosen', 'mapel')->where($payload);
            $count = $data->count();
        }

        if (!isset($all)) {
            $data = $data->skip($offset)->take($limit);
        }

        $data = $data->orderBy('created_at', 'desc')->get();
        $status = ResponseStatus::STATUS_OK;

        return response()->json(compact('data', 'status', 'count'), ResponseStatus::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'max' => 'required',
            'dosen_id' => 'required',
            'mapel_id' => 'required'
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'max', 'dosen_id', 'mapel_id', 'active']);

        // untuk menanggulangi dosen yang sama telah punya mapel yang sama
        $payload['kode'] = 'MK-' . $request->get('mapel_id') . '#' . 'D' . $request->get('dosen_id');
        $data = Kelas::create($payload);
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function checkMahasiswaSubscribed(Request $request)
    {
        $mhs_id = $request->query("mhs_id");
        $kelas_id = $request->query("kelas_id");
        if (!isset($mhs_id) || !isset($kelas_id)) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = [['mahasiswa_id', '=', $mhs_id], ['kelas_id', '=', $kelas_id]];
        $data = MahasiswaKelas::where($payload)->count();

        return response()->json(['data' => $data > 0, 'status' => ResponseStatus::STATUS_OK], ResponseStatus::HTTP_OK);
    }

    public function subscribe_class(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mahasiswa_id' => 'required',
            'kelas_id' => 'required',
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $kelas = Kelas::find($request->get('kelas_id'));
        $count_mhs_kelas = MahasiswaKelas::where('kelas_id', $kelas->id)->count();

        // check apakah tidak melebihi kuota
        if ($count_mhs_kelas >= $kelas->max) {
            $message = 'Kelas ini sudah penuh!';
            $status = ResponseStatus::OPERATION_FAILED;

            return response()->json(compact('message', 'status'), ResponseStatus::HTTP_FORBIDDEN);
        }

        $payload = $this->generatePayloadFromRequest($request, ['mahasiswa_id', 'kelas_id']);

        // untuk menanggulangi mhs yang sama telah punya kelas yang sama
        $payload['kode'] = 'MK-' . $request->get('kelas_id') . '#' . 'M' . $request->get('mahasiswa_id');

        $data = MahasiswaKelas::create($payload);
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $kelas = Kelas::find($request->get('id'));
        if ($kelas->active) {
            $message = 'Tidak dapat mengupdate, Kelas sudah diaktifkan oleh dosen pengampu!';
            $status = ResponseStatus::OPERATION_FAILED;

            return response()->json(compact('message', 'status'), ResponseStatus::HTTP_FORBIDDEN);
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'dosen_id', 'max']);
        $data = $kelas->update($payload);
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function activate_class(Request $request)
    {
        $kelas_id = $request->get('id');

        if ($kelas_id === null) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $count_mhs_kelas = MahasiswaKelas::where('kelas_id', $kelas_id)->count();
        if ($count_mhs_kelas < 6) {
            $message = 'Tidak bisa mengaktifkan kelas, min siswa 6';
            $status = ResponseStatus::OPERATION_FAILED;

            return response()->json(compact('message', 'status'), ResponseStatus::HTTP_FORBIDDEN);
        }

        $data = Kelas::find($kelas_id)->update(['active' => '1']);
        $status = ResponseStatus::UPDATE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }
}
