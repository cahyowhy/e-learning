<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\ResponseStatus;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $offset = $request->query("offset");
        $limit = $request->query("limit");
        $name = $request->query("name");
        $role_id = $request->query("role_id");
        $email = $request->query("email");
        $kelas_id = $request->query("kelas_id");
        $status = ResponseStatus::STATUS_OK;

        if ($kelas_id !== null) {
            $data = User::whereHas('mahasiswa_kelas', function ($q) use ($kelas_id) {
                $q->where('kelas_id', $kelas_id);
            })->with('role')->get();

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
        }

        if (!isset($offset) || !isset($limit)) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = [];
        if (isset($role_id)) {
            array_push($payload, ['role_id', '=', $role_id]);
        }

        if (isset($name)) {
            array_push($payload, ['name', 'LIKE', '%' . $name . '%']);
        }

        if (isset($email)) {
            array_push($payload, ['email', '=', $email]);
        }

        $result = User::with('role')->where($payload);
        $count = $result->count();
        $data = $result->skip($offset)->take($limit)->orderBy('created_at', 'desc')->get();

        return response()->json(compact('data', 'status', 'count'), ResponseStatus::HTTP_OK);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $status = ResponseStatus::LOGIN_SUCCESS;

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $payload = ['data' => ResponseStatus::STATUS_TEXT[401], 'status' => ResponseStatus::INPUT_NOT_VALID];

                return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
            }
        } catch (JWTException $e) {
            $payload = ['data' => $e->getMessage(), 'status' => ResponseStatus::SYSTEM_EXCEPTION];

            return response()->json($payload, ResponseStatus::HTTP_INTERNAL_SERVER_ERROR);
        }

        $data = User::with('role')->where('email', $request->get('email'));

        if (empty($data)) {
            $data = ResponseStatus::STATUS_TEXT[404];
            $status = ResponseStatus::DATA_NOT_FOUND;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_NOT_FOUND);
        }

        $data = $data->first();
        $data["token"] = $token;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }

    public function update_user_password(Request $request)
    {
        $validation_param = [
            'password' => 'required', 'passwordOld' => 'required', 'id' => 'required'
        ];
        $validator = Validator::make($request->all(), $validation_param);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $user = User::find($request->get('id'));
        if (Hash::check($request->get('passwordOld'), $user->password)) {
            $user->update(['password' => Hash::make($request->get('password'))]);

            return response()->json(['status' => ResponseStatus::UPDATE_SUCCESS], ResponseStatus::HTTP_OK);
        }

        $data = ['status' => ResponseStatus::PASSWORD_NOT_VALID, 'message' => ResponseStatus::STATUS_TEXT[401]];

        return response()->json($data, ResponseStatus::HTTP_UNAUTHORIZED);
    }

    public function register(Request $request)
    {
        $validation_param = [
            'name' => 'required|string', 'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6', 'role_id' => 'required'
        ];
        $validator = Validator::make($request->all(), $validation_param);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'email', 'role_id', 'phone_number', 'address']);
        $payload['password'] = Hash::make($request->get('password'));

        $data = User::create($payload);
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'address', 'phone_number', 'email']);
        User::find($request->get('id'))->update($payload);

        $data = User::with('role')->find($request->get('id'));
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function upload_image_profile(Request $request, $id)
    {
        $payload = [];
        $status = ResponseStatus::UPLOAD_SUCCESS;
        $user = User::find($id);
        if ($request->file('file') !== null) {

            if (strlen($user->image_profile) > 0) {
                $this->unlink(public_path('user-files/users/' . $user->image_profile));
            }

            $filename = $this->uploadFile('file', 'users', '1mb', true);
            if (strlen($filename) > 0) {
                $payload['image_profile'] = $filename;
                $user->update($payload);

                $data = User::with('role')->find($id);

                return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
            }

            return $this->response_bad_request();
        }

        return $this->response_bad_request();
    }
}
