<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mapel;
use App\ResponseStatus;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index(Request $request)
    {
        $data = Mapel::all();
        $status = ResponseStatus::STATUS_OK;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }

    public function create(Request $request)
    {
        $data = [];
        $status = ResponseStatus::SAVE_SUCCESS;
        $payload = $this->generateParamFromFormData($request->get('mapel'), ['name', 'id']);

        if (!isset($payload['name'])) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $is_edit = !empty($payload['id']);
        if ($request->file('file') !== null) {
            if ($is_edit) { // if is edit check if has file before this
                $mapel = Mapel::find($payload['id']);

                if (strlen($mapel->image) > 0) {
                    $this->unlink(public_path('user-files/mapels/' . $mapel->image));
                }
            }

            $filename = $this->uploadFile('file', 'mapels', '1mb', true);
            if (strlen($filename) > 0) {
                $payload['image'] = $filename;
            }
        }

        if ($is_edit) {
            $data = Mapel::find($payload['id'])->update($payload);
        } else {
            $data = Mapel::create($payload);
        }

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }
}
