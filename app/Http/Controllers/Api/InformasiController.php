<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Informasi;
use App\ResponseStatus;
use Illuminate\Support\Facades\DB;

class InformasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offset = $request->query("offset");
        $limit = $request->query("limit");

        if (!isset($offset) || !isset($limit)) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $data = DB::table('informasis')->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
        $status = ResponseStatus::STATUS_OK;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'description' => 'required|string',
        ]);

        $data = [];

        if ($validator->fails()) {
            $data = $validator->errors()->toJson();
            $status = ResponseStatus::INPUT_NOT_VALID;

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_BAD_REQUEST);
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'description', 'published']);

        if ($request->get('id') !== null) {
            $data = Informasi::find($request->get('id'))->update($payload);
        } else {
            $data = Informasi::create($payload);
        }

        $status = ResponseStatus::SAVE_SUCCESS;
        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }
}
