<?php

namespace App\Http\Controllers\Api;

use App\MahasiswaSoal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Soal;
use App\MahasiswaKelas;
use App\ResponseStatus;
use App\Util\LcgDistribution;

class SoalController extends Controller
{
    public function index(Request $request)
    {
        $kelas_id = $request->query("kelas_id");
        $mhs_id = $request->query("mahasiswa_id");
        $id = $request->query("id");
        $status = ResponseStatus::STATUS_OK;
        $data = [];

        if (!isset($kelas_id)) {
            return $this->response_bad_request();
        }

        $payload = [['kelas_id', '=', $kelas_id]];
        if ($id !== null) {
            $data = MahasiswaSoal::where('soal_id', $id)->with('soal', 'mahasiswa')->get();
        } else if ($mhs_id !== null) {
            array_push($payload, ['mahasiswa_id', '=', $mhs_id]);
            $data = MahasiswaSoal::whereHas('soal', function ($q) use ($payload) {
                $q->where($payload);
            })->with('soal', 'mahasiswa')->get();
        } else {
            $data = Soal::where($payload)->with('kelas')->get();
        }

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }

    public function min_max_soal_mhs_given(Request $request)
    {
        $kelas_id = $request->query("kelas_id");

        if (!isset($kelas_id)) {
            return $this->response_bad_request();
        }

        $countMhsKelas = MahasiswaKelas::where('kelas_id', $kelas_id)->count();
        $countMax = $countMhsKelas % 2 === 0 ? $countMhsKelas : ($countMhsKelas + 1);
        $countMin = $countMax % 2 === 0 ? ($countMax / 2) : (($countMax / 2) + 1);
        $data = ['max' => $countMax, 'min' => $countMin];
        $status = ResponseStatus::STATUS_OK;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }

    public function create(Request $request)
    {
        $data = [];
        $status = ResponseStatus::UPLOAD_SUCCESS;
        $payload = $this->generateParamFromFormData($request->get('soal'), ['name', 'description', 'due', 'kelas_id']);
        $file_names = [];

        if (empty($payload['name']) || empty($payload['kelas_id']) || empty($payload['description'])) {
            $payload = ['data' => ResponseStatus::STATUS_TEXT[400], 'status' => ResponseStatus::INPUT_NOT_VALID];

            return response()->json($payload, ResponseStatus::HTTP_BAD_REQUEST);
        }

        $soal = Soal::create($payload);
        $mahasiswa_kelases = MahasiswaKelas::where('kelas_id', $payload['kelas_id'])->get()->toArray();

        if (count($mahasiswa_kelases) < 6) {
            $message = 'Tidak bisa memproses, Siswa di kelas ini Harus >= 6';
            $status = ResponseStatus::OPERATION_FAILED;

            return response()->json(compact('message', 'status'), ResponseStatus::HTTP_FORBIDDEN);
        }

        if (count($_FILES) > 0) {
            foreach ($_FILES as $key => $value) {
                $filename = $this->uploadFile($key, 'soals', '2mb');
                if (strlen($filename) > 0) {
                    array_push($file_names, $filename);
                } else {
                    // hacky rollback. unlink all uploaded file first then return
                    foreach ($file_names as $path) {
                        $this->unlink(public_path('user-files/soals/' . $path));
                    }

                    return response()->json(['status' => ResponseStatus::INPUT_NOT_VALID], ResponseStatus::HTTP_BAD_REQUEST);
                }
            }
        }

        $mahasiswa_soal_payloads = LcgDistribution::generate_soals_distribution($mahasiswa_kelases, $file_names, $soal->id);
        $data = MahasiswaSoal::insert($mahasiswa_soal_payloads);

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->response_bad_request();
        }

        $payload = $this->generatePayloadFromRequest($request, ['name', 'description', 'due']);
        $data = Soal::find($request->get('id'))->update($payload);
        $status = ResponseStatus::SAVE_SUCCESS;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_CREATED);
    }

    public function update_mahasiswa_soal(Request $request)
    {
        $data = [];
        $status = ResponseStatus::UPLOAD_SUCCESS;
        $payload = $this->generateParamFromFormData($request->get('mahasiswa_soal'), ['id']);

        if (empty($payload['id']) || $request->file('file') === null) {
            return $this->response_bad_request();
        }

        $filename = $this->uploadFile('file', 'soals', '2mb');
        if (strlen($filename) > 0) {
            $data = MahasiswaSoal::find($payload['id'])->update(['download_path' => $filename]);

            return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
        }

        return $this->response_bad_request();
    }
}
