<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\ResponseStatus;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Role::all();
        $status = ResponseStatus::STATUS_OK;

        return response()->json(compact('data', 'status'), ResponseStatus::HTTP_OK);
    }
}
