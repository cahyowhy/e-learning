<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use App\ResponseStatus;
use Cookie;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $from_api = mb_strpos($request->fullUrl(), '/api/') !== false;

        try {
            if ($from_api) {
                $user = JWTAuth::parseToken()->authenticate();
            }

            return $next($request);
        } catch (Exception $e) {
            return response()->json(['status' => ResponseStatus::TOKEN_EXPIRED], ResponseStatus::HTTP_UNAUTHORIZED);
        }
    }
}
