<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    public function dosen()
    {
        return $this->belongsTo('App\User', 'dosen_id', 'id');
    }

    public function mapel()
    {
        return $this->belongsTo('App\Mapel');
    }

    protected $fillable = [
        'name', 'max', 'dosen_id', 'mapel_id', 'active', 'kode'
    ];

    public function scopeJoin($query)
    {
        return $query->with('dosen', 'mapel');
    }

    public function mahasiswa_kelas()
    {
        return $this->hasMany('App\MahasiswaKelas');
    }
}
