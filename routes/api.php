<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users', 'Api\UserController@register');
Route::post('login', 'Api\UserController@authenticate');

// role
Route::get('roles', 'Api\RoleController@index');

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('informasis', 'Api\InformasiController@index');
    Route::post('informasis', 'Api\InformasiController@create');

    Route::put('users', 'Api\UserController@update');
    Route::post('users/upload-image-profile/{id}', 'Api\UserController@upload_image_profile');
    Route::get('users', 'Api\UserController@index');
    Route::post('users/update-password', 'Api\UserController@update_user_password');

    Route::get('mapels', 'Api\MapelController@index');
    Route::post('mapels', 'Api\MapelController@create');

    Route::get('kelas', 'Api\KelasController@index');
    Route::post('kelas', 'Api\KelasController@create');
    Route::put('kelas', 'Api\KelasController@update');
    Route::post('kelas/subscribe', 'Api\KelasController@subscribe_class');
    Route::get('kelas/check-kelas-subscribe', 'Api\KelasController@checkMahasiswaSubscribed');
    Route::get('kelas/activate', 'Api\KelasController@activate_class');

    Route::get('soals', 'Api\SoalController@index');
    Route::post('soals', 'Api\SoalController@create');
    Route::put('soals', 'Api\SoalController@update');
    Route::post('soals/update-mahasiswa-soals', 'Api\SoalController@update_mahasiswa_soal');
    Route::get('soals/min-maxs', 'Api\SoalController@min_max_soal_mhs_given');
});
