<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BackupAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->timestamps();
        });

        // users
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('image_profile')->nullable();
            $table->string('email', 50)->unique();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->string('password');
            $table->timestamps();
        });

        $this->createUserScheme();

        // mapels
        Schema::create('mapels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 25);
            $table->string('image')->nullable();
            $table->timestamps();
        });

        // kelas
        Schema::create('kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 15);
            $table->integer('max');
            $table->boolean('active')->default(0);
            $table->string('kode')->unique();
            $table->timestamps();
        });

        $this->createKelasScheme();

        // soals
        Schema::create('soals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 25);
            $table->text('description');
            $table->boolean('published')->default(1);
            $table->timestamp('due');
            $table->timestamps();
        });

        $this->createSoalScheme();

        // mahasiswa soals
        Schema::create('mahasiswa_soals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('kode')->unique();
            $table->string('download_path');
        });

        $this->createMahasiswaSoalScheme();

        // informasi
        Schema::create('informasis', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('published')->default(1);
            $table->string('name', 50);
            $table->text('description');
        });

        // MahasiswaKelas
        Schema::create('mahasiswa_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('approved')->default(1);
            $table->string('kode')->unique();
            $table->timestamps();
        });

        $this->createMahasiswaKelasScheme();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    /**
     * create user scheme
     */
    public function createUserScheme()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function createMahasiswaKelasScheme()
    {
        Schema::table('mahasiswa_kelas', function (Blueprint $table) {
            $table->integer('mahasiswa_id')->unsigned();
            $table->foreign('mahasiswa_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('kelas_id')->unsigned();
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * create soal scheme
     */
    public function createSoalScheme()
    {
        Schema::table('soals', function (Blueprint $table) {
            $table->integer('kelas_id')->unsigned();
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * create soal scheme
     */
    public function createMahasiswaSoalScheme()
    {
        Schema::table('mahasiswa_soals', function (Blueprint $table) {
            $table->integer('soal_id')->unsigned();
            $table->foreign('soal_id')->references('id')->on('soals')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('mahasiswa_id')->unsigned();
            $table->foreign('mahasiswa_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * create soal scheme
     */
    public function createKelasScheme()
    {
        Schema::table('kelas', function (Blueprint $table) {
            $table->integer('dosen_id')->unsigned();
            $table->foreign('dosen_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('mapel_id')->unsigned();
            $table->foreign('mapel_id')->references('id')->on('mapels')->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
