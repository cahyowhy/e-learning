<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class InformasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(0,50) as $i){
            DB::table('informasis')->insert([
                'name' => $faker->name,
                'description' => "<p>".$faker->text."</p>",
                'created_at' => $faker->dateTime(),
            ]);
        }
    }
}
