<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->dosens();
        $this->mahasiswas();
    }

    public function dosens()
    {
        $faker = Faker::create();
        foreach (range(0, 5) as $i) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('123456'),
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role_id' => 2
            ]);
        }
    }

    public function mahasiswas()
    {
        $faker = Faker::create();
        foreach (range(0, 60) as $i) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'password' => Hash::make('123456'),
                'role_id' => 3
            ]);
        }
    }
}
