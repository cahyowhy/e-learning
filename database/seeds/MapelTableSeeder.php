<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MapelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mapels = ['Matematika Diskrit', 'Bahasa Indonesia', 'Agama Islam', 'Multimedia', 'Pancasila',
            'Kewarganegaraan', 'Matematika Dasar'];

        foreach ($mapels as $mapel) {
            DB::table('mapels')->insert([
                'name' => $mapel
            ]);
        }
    }
}
