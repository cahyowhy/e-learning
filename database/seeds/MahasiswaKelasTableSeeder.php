<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MahasiswaKelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mahasiswas = DB::table('users')->where('role_id', 3)
            ->offset(0)->limit(12)->get();
        $mahasiswa_ids = [];

        foreach ($mahasiswas as $mahasiswa) {
            array_push($mahasiswa_ids, $mahasiswa->id);
        }

        $kelases = DB::table('kelas')->get();
        $kelas_ids = [];

        foreach ($kelases as $kelas) {
            array_push($kelas_ids, $kelas->id);
        }

        foreach ($mahasiswa_ids as $index_mhs => $mhs_id) {
            foreach ($kelas_ids as $index_kelas => $kelas_id) {
                $kode = 'MK-' . $kelas_id . '#' . 'M' . $mhs_id;
                DB::table('mahasiswa_kelas')->insert([
                    'kode' => $kode,
                    'mahasiswa_id' => $mhs_id,
                    'kelas_id' => $kelas_id
                ]);
            }
        }
    }
}
