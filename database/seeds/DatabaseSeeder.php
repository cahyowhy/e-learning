<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->runFirstTime();
        $this->runEveryTime();
    }

    /**
     * jalankan ini hanya jika seluruh tabel telah kosong
     * sehabis db di drop
     */
    public function runFirstTime()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(MapelTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(KelasTableSeeder::class);
        $this->call(MahasiswaKelasTableSeeder::class);
    }

    /**
     * jalankan semaumu
     */
    public function runEveryTime()
    {
        $this->call(InformasiTableSeeder::class);
    }
}
