<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dosens = DB::table('users')->where('role_id', 2)->get();
        $dosen_ids = [];

        foreach ($dosens as $dosen) {
            array_push($dosen_ids, $dosen->id);
        }

        $mapels = DB::table('mapels')->get();
        $mapel_ids = [];

        foreach ($mapels as $mapel) {
            array_push($mapel_ids, $mapel->id);
        }

        foreach ($dosen_ids as $index_dosen => $dosen_id) {
            foreach ($mapel_ids as $index_mapel => $mapel_id) {
                $kode = 'MK-' . $mapel_id . '#' . 'D' . $dosen_id;
                DB::table('kelas')->insert([
                    'name' => 'TI' . $index_dosen . '-' . $mapel_id,
                    'kode' => $kode,
                    'max' => 15,
                    'dosen_id' => $dosen_id,
                    'mapel_id' => $mapel_id
                ]);
            }
        }
    }
}
